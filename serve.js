'use strict';
const express = require('express');
const fs = require('fs');
const templates = require('./lib/templates.js');
const metalsmith = require('./metalsmith.js');
const email = require('./lib/email.js');
const gitlab = require('./lib/gitlab.js');
const git = require('./lib/git.js')({});
const loggers = require('./lib/logging.js').loggers;
const config = require('./lib/config.js');
const spamTrap = require('./lib/spam-trap.js')(config.spamTraps);

const buildlog = loggers.build;
const httplog = loggers.http;
const hooklog = loggers.hook;
const submitlog = loggers.submit;
const maillog = loggers.mail;

const send = email(config.email.server);
const app = express();
const port = config.port;


var expressOptions = {
    dotfiles: 'ignore',
    etag: false,
    //  extensions: ['htm', 'html'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set('x-timestamp', Date.now());
    }
};

var fileOptions = {
    root: __dirname + '/build/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true,
    }
};

async function build() {
    buildlog.info('starting build...')
    metalsmith().build(function(err) {
        if (err)
            buildlog.error('build exception', err);
    })
    buildlog.info('completed build.')
}

app.set('trust proxy', true); // Assume we're behind a proxy
app.use(express.static('/', expressOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.all('*', (req, res, next) => {
    next();
    httplog.info(`${req.method} ${req.url} ${req.hostname} ${req.ip} ${res.statusCode}`, {
        method: req.method,
        url: req.url,
        ip: req.ip,
        host: req.hostname,
        code: res.statusCode,
    });
})
app.get('/', (req, res) => {
    res.redirect('/home.html');
});
app.get('/style.css', (req, res) => {
    res.sendFile(req.path, fileOptions);
});
app.get('*', (req, res, next) => {
    var path = req.path+'/index.html';
    if (fs.existsSync(fileOptions.root+path)) {
        // Cater to the wiki's nested-indexes style
        res.sendFile(path, fileOptions);
        return;
    }
    if (fs.existsSync(fileOptions.root+req.path)) {
        res.sendFile(req.path, fileOptions);
        return;
    }
    path = req.path+'.html';
    if (fs.existsSync(fileOptions.root+path)) {
        res.redirect(path);
        return;
    }
    next();
});

if (!config.webhook) {
    hooklog.warn("No webhook configured!");
}
else{
    app.post(config.webhook.path, (req, res) => {
        var secret = req.header('X-Gitlab-Token');
        if (secret !== config.webhook.secret) {
            res.sendStatus(403); // Unauthorised
            hooklog.warn(`rejecting unauthorised hook call`);
            return;
        }
        
        try {
            hooklog.info(`handling hook call by pulling`);
            git // Async
                .pull()
                .then(build)
                .then(() => hooklog.info(`handling hook call complete`));
        }
        catch(e) {
            hooklog.error('error handling hook', e);
        }
        res.sendStatus(200);
    });
}
app.post('/registration', (req, res) => {
    const sendConfig = config.email.registration || {};
    const templateContext = {
        config: {theme: config.theme},
        request: req,
    };
    
    function ok() {}
    function error() {
        return (err) => submitlog.info(`$message: `, err);
    }

    if (req.body.email) {
        // Send acknowledgement email to registrant
        const acknowledgementConfig = {
            from: sendConfig.from,
            to: req.body.email,
            subject: sendConfig.acknowledgement,
        };
        try {
            const html = templates.acknowledgement(templateContext);
            submitlog.info(`emailing registration acknowledgement to ${acknowledgementConfig.to}: `);
            send(acknowledgementConfig,
                 html)
                .then(ok, error("error emailing registration acknowledgement"));
        }
        catch(e) {
            maillog.error(`failed to send registration acknowledgement to ${acknowledgementConfig.to} `, e);
        }
    }
    else {
        maillog.error(`cannot send registration acknowledgement, no email set in registration`);
    }

    // This function logs spamtrap hits, we just need to drop spam
    // (transparently to the spammer).  i.e. We send the registration
    // notification (above), but nothing else.
    if (!spamTrap(req.body)) {
        if (gitlab) {
            try {
                gitlab.newRegistrationIssue(templateContext)
                    .then(ok, error("error posting new gitlab issue"));
            }
            catch(e) {
                submitlog.info('error posting new gitlab issue: ', e);
            }
        }

        // Send notification of registration to admins
        submitlog.info(`emailing registration notification to ${sendConfig.to}`, req.body);
        const notificationConfig = {
            from: sendConfig.from,
            to: sendConfig.to,
            subject: sendConfig.notification,
        };
        try {
            const html = templates.notification(templateContext);
            send(notificationConfig,
                 html,
                 req.body)
                .then(ok, error("error emailing registration notification"));
        }
        catch(e) {
            maillog.error(`failed to send notification of registration to ${notificationConfig.to}: `, e);
        }
    }
    res.redirect('/home');
});

app.listen(port, () => console.log(`serve.js listening on port ${port}!`));
