module.exports = {
    theme: {
        timezone: 'Europe/London',
        title: 'Example Title',
        description: 'Example description',
        logo: 'images/example_logo.svg',
        urls: {
            // The site home URL - logo links to this
            home: 'https://example.com',
            // The top level url of the wiki
            base: 'https://example.com/wiki',
            // An url to prepend author usernames to
            authorbase: 'https://git.coop/',
        },
    },    
    port: 9999,
    email: {
        server: {
            user: "example",
            password: "secret!",
            host: "mail.example.com",
            port: 25,
            timeout: 15000, // (Default tends to need to be increased)
            tls: true,
        },
        registration: {
            // REQUIRED fields!

            // Where replies to notifications/acknowledgements go
            from: "Registrations <registrations@example.com>", 
            // Where notifications go (acknowledgements go to the
            // registrant).
            to: "Registrations <registrations@example.com>",
            // The subject line for notifications
            notification: "Someone has registered!",
            // The subject line for acknowledgements
            acknowledgement: "Thanks for registering!",
        },
        templates: {
            // Note: recommended place for templates is in src/email-templates/.
            // notification: 'src/email-templates/notification-template.hbs',
            // acknowledgement: 'src/email-templates/acknowledgement-template.hbs',
            // Theme config file included in template context under 'config' key.
            // Also, request under 'request'
        },
    },
    gitlab: {
        server: {
            issuesUrl: 'https://example.com/api/v4/projects/9/issues',
            token: 'sekret',
        },
        registration: {
            title: 'Registration for {{oc-user}}',
            description: './src/templates/issue.hbs',
        },
    },
    webhook: {
        path: '/example-path',
        secret: 'secret!',
    },
    logging: {
    },
}
