'use strict';
const Metalsmith = require('metalsmith');
const assets = require('metalsmith-assets-improved');
const collections = require('metalsmith-collections');
const layouts = require('metalsmith-layouts');
const markdown = require('metalsmith-markdown');
const permalinks = require('metalsmith-permalinks');
const metalsmithDebug = require('metalsmith-debug');
const discoverPartials = require('metalsmith-discover-partials');
const singular = require('pluralize').singular;
const winston = require('winston');
const fs = require('fs');
const helpers = require('./lib/helpers.js');
const git = require('./lib/git.js')();
const debug = require('debug')('metalsmith.js');
const config = require('./lib/config.js');
const buildlog = require('./lib/logging.js').loggers.build;

/** Iterates over the files, applying func to each */
function forFiles(onFile) {
    return function(files, metalsmith) {
        for(const file in files) {
            const data = files[file];
            // console.log(file, data)
            onFile(data, file, metalsmith);
        }
    };
}

/** Iterates over the files, adding metadata which isn't already set */
function addMeta(meta) {
    return forFiles((data, file, metalsmith) => {
        for(const key in meta) {
            if (key in data)
                continue;

            var val = meta[key];
            if (typeof(val) === 'function')
                val = val(data, file, metalsmith);
            if (val !== undefined)
                data[key] = val;
        }
    });
}

/** Converts a path to a title, so titles don't need to be defined */
function pathToTitle(path, truncate = true) {
    var path = path.split('/')
    if (truncate && path.length)
        path = path.splice(path.length-1);
    return path.map(
        c =>
            c.replace(/[.](md|html)$/, '')
            .replace(/^(.)/, (_, ch) => ch.toUpperCase())
            .replace(/-(.?)/g, (_, ch) => ' '+ch.toUpperCase())
    ).join('/');
}

/** Extract a layout name if the file is part of a collection, and an
 * appropriate one exists */
function layoutFromCollection(data, opts = {}) {
    if (!opts.extension)
        opts.extension = '.hbs';
    if (!opts.layoutsDir)
        opts.layoutsDir = 'theme';

    if (data.collection instanceof Array &&
        data.collection.length > 0) {
        var name = singular(data.collection[0]);
        // Get the first collection item, ignore the others.
        var layout = `${name}${opts.extension}`;

        // Only use it if there is a corresponding layout file
        if (fs.existsSync(`${__dirname}/${opts.layoutsDir}/${layout}`))
            return layout;
    }
}

/** Plugin to add collection index pages */
function collectionIndexes(opts = {}) {
    return function(files, metalsmith) {
        const meta = metalsmith.metadata();
        for(var indexfile in opts) {
            var indexopts = opts[indexfile];
            if (typeof(indexopts) !== 'object')
                indexopts = {collection: indexopts};

            const index = meta.collections[indexopts.collection];
            if (!index)
                continue;

            const layout = indexopts.layout ||
                  `${indexopts.collection}-index`;
            files[indexfile] = {
                contents: Buffer.alloc(0),
                layout: layout,
                filename: indexfile,
                path: indexfile,
            };
        }
    };
}

const gitMeta = git.fileInfoSync();

// A function to compare posts by git.first.authored
function postCreation(a, b) { 
    a = gitMeta[a.path];
    b = gitMeta[b.path];
    a = a? a.first.authored : 0;
    b = b? b.first.authored : 0;
    return b - a;
}

// Dummy metadata to omit voluminous/circular crap fields
function elideMeta(meta) {
    return {
            ...meta,
        contents: '...omitted',
        next: meta.next && meta.next.path,
        previous: meta.previous && meta.previous.path,
    };
}

// We return a function building the metalsmith object.
// This works around a metalsmith bug when run in a persistent environment.
// https://github.com/segmentio/metalsmith-collections/issues/27
// Another solution here:
// https://destinmoulton.com/blog/2018/metalsmith-collections-solving-the-duplication-bug/
module.exports = () => {
    return Metalsmith(__dirname)
    .metadata({
    })
    .ignore([
        '**/*~',  // Emacs backup files
        '.*',     // dotfiles
        'templates', // templates directory
    ])
    .clean(true)                // clean destination before building
    .use(metalsmithDebug())
    .use(collections({
        posts: {
            pattern: 'posts/*.md',
            sortBy: postCreation,
        },
        pages: {
            pattern: ['**/*.md','!posts/*.md'],
        },
    }))
    .use(collectionIndexes({
        'posts/index.html': {
            collection: 'posts',
            layout: 'index.hbs',
        },
        'index.html': {
            collection: 'pages',
            layout: 'sitemap.hbs',
        },
    }))
    .use(markdown())
    .use(addMeta({
        config: config.theme,
        title: (d) => pathToTitle(d.path),
        longtitle: (d) => pathToTitle(d.path, false),
        layout: (d) => layoutFromCollection(d),
        filename: (d, f) => {
            // set path to file path, set filename to original path
            d._path = d.path;
            d.path = f;            
            return d._path;
        },
        git: (d) => gitMeta[d._path],
    }))
    .use(discoverPartials({
        directory: 'theme/partials',
        pattern: /\.hbs$/
    }))
    .use(layouts({
        default: 'page.hbs',
        directory: 'theme',
        engineOptions: {
            helpers: helpers,
        },
    }))
    .use(forFiles((d,f) => buildlog.silly(f,elideMeta(d))))
    .use(assets({
        src: 'theme/assets',
    }));
};

if (require.main === module) {
    // Called directly, build
    module.exports().build(function(err) {
        if (!err) return;
        console.error('Build failed: ', err);
        process.exit(1);
    });
}

