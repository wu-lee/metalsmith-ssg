'use strict';
const axios = require('axios');
const submitlog = require('./logging.js').loggers.submit;
const config = require('./config.js').gitlab;
const templates = require('./templates.js');

if (!config) {
    // We can't do anything
    module.exports = null;
    return;
}

const {server, registration} = config;
if (!server.issuesUrl)
    throw new Error("there must be a gitlab.server.issuesUrl value configured");

if (!server.token)
    throw new Error("there must be a gitlab.server.token value configured");

module.exports = {
    newRegistrationIssue: function(context) {
        const {request, config} = context;
        const body = request.body;
        
        function onsuccess(response) {
            const id = response.data.iid;
            submitlog.info(`added a new ticket ${id} for registration by ${body['oc-user']}`);
        }
        function onfailure(error) {
            submitlog.info(`failed to add a new ticket for registration by ${body['oc-user']}. `, error);
        }
        
        return axios.post(
            server.issuesUrl,
            {
                title: `Registration for ${body['oc-user']}`,
                confidential: true,
                description: templates.issue(context),
            },
            { headers: {'PRIVATE-TOKEN': server.token} },
        ).then(onsuccess, onfailure);
    },
};
