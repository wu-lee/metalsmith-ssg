'use strict';
/** These are default values which any user-supplied values override.
 * See also example-config.js, for what that might be. 
 */
module.exports = {
    port: 9999,
    email: {},
    webhook: {
        path: '/',
        secret: '',
    },
    logging: {
        level: 'info',
    },
};
