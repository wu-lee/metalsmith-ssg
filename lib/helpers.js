const Handlebars = require('handlebars');
const moment = require('moment-timezone');

function a(text, path, baseurl) {
    const url = new URL(path, baseurl);
    text = Handlebars.Utils.escapeExpression(text);
    return `<a href="${url}">${text}</a>`;
}

function getopts(args) {
    if (args.length > 0) {
        args.length -= 1;
        const opts = args[args.length];
        delete args[args.length];
        return Array.prototype.concat.apply([opts], args);
    }
    
    return [{}];
}

module.exports = {
    wikiLink: function() {
        const [opts, path] = getopts(arguments);
        // This requires wikiurl to be defined in global metadata
        path = path.replace(/[.]md$/, '');
        path = path.replace(/^\//, '');
        const wikiurl = opts.data.root.wikiurl.replace(/\/+$/,'/');
        return new Handlebars.SafeString(wikiurl+path);
    },

    authorLink: function() {
        const [opts, author] = getopts(arguments);
        const link = a(author, author, opts.data.root.config.urls.authorbase);
        return new Handlebars.SafeString(link);
    },

    /** Html escapes text, but converts adjacent newlines into paragraph tags */
    parags: function() {
        var [opts, text] = getopts(arguments);
        text = Handlebars.Utils.escapeExpression(text);
        text = text.replace(/\n\s*\n/mg, '<p>');
        return new Handlebars.SafeString(text);
    },

    /** Markdown quote text */
    mdquote: function() {
        var [opts, text] = getopts(arguments);
        text = (text || '').replace(/\r/mg, '').replace(/^/mg, '> ');
        return new Handlebars.SafeString(text);
    },

    /** Json output */
    json: function() {
        var [opts, data] = getopts(arguments);
        data = JSON.stringify(
            data, null,
            opts.hash.pretty? Number(opts.hash.indent || 2) : null
        );
        return new Handlebars.SafeString(data);
    },

    // Ghost emulations
    asset: function(path) {
        return '/'+path;
    },
    content: function() {
        const [opts] = getopts(arguments);
        return opts.data.root.contents;
    },
    'is-in': function() {
        var [options, types] = getopts(arguments);
        
        const collections = options.data.root.collection;

        if (collections) {
            switch(typeof types) {
            case 'string':
                types = types.split(',').map(s => s.trim());
                break;
            case 'array':
                break;
            case 'undefined':
                throw new Error("#is helper must have a parameter");
            default:
                throw new Error("#is helper parameter must be comma-delimited string or array");
            }
            
            // search collections and types to find a matching type
            const ismatch = types.reduce(
                (result, type) => result || collections.indexOf(type) >= 0,
                false
            );
            
            if (ismatch) {
                return options.fn(this);
            }
        }

        return options.inverse(this);
    },
    foreach: function(items, options) {
        // we need options
	    if (!options) {
	        throw new Error('Must pass iterator to #foreach');
	    }

        function filterItemsByVisibility(items, options) {
            return items; // stub, reimplement later
        }

	    if (options.data && options.ids) {
	      contextPath = Handlebars.Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
	    }

        if (typeof(items) === 'function') {
            items = items.call(this);
        }

        // Exclude items which should not be visible in the theme
        items = filterItemsByVisibility(items, options);

        // Initial values set based on parameters sent through. If
        // nothing sent, set to defaults
        var fn = options.fn,
            inverse = options.inverse,
            columns = options.hash.columns,
            length = items.length,
            limit = Math.floor(options.hash.limit) || length,
            from = Math.floor(options.hash.from) || 1,
            to = Math.floor(options.hash.to) || length,
            output = '',
            data,
            contextPath;

        // If a limit option was sent through (aka not equal to
        // default (length)) and from plus limit is less than the
        // length, set to to the from + limit
        if ((limit < length) && ((from + limit) <= length)) {
            to = (from - 1) + limit;
        }
        
        if (options.data) {
            data = Handlebars.createFrame(options.data);
        }
        
        function execIteration(field, index, last) {
            if (data) {
                data.key = field;
                data.index = index;
                data.number = index + 1;
                data.first = index === from - 1; // From uses 1-indexed, but array uses 0-indexed
                data.last = !!last;
                data.even = index % 2 === 1;
                data.odd = !data.even;
                data.rowStart = index % columns === 0;
                data.rowEnd = index % columns === (columns - 1);
            }

            output = output + fn(items[field], {
                data: data,
                blockParams: Handlebars.Utils.blockParams([items[field], field], [contextPath + field, null])
            });
        }

        function iterateCollection(context) {
            // Context is all posts on the blog
            var current = 1;

            // For each post, if it is a post number that fits within
            // the from and to, send the key to execIteration to be
            // added to the page
            context.forEach((item, key) => {
                if (current < from) {
                    current += 1;
                    return;
                }

                if (current <= to) {
                    execIteration(key, current - 1, current === to);
                }
                current += 1;
            });
        }

        if (items && typeof items === 'object') {
            iterateCollection(items);
        }

        return output;
    },
    post: function() {
        const [opts] = getopts(arguments);
        if (opts.fn) {
            const str = opts.fn(this, opts.data);
            //console.log("inner", this, arguments, `(${str})`);
            return str;
        }
        else {
            return opts.post;
        }
    },
    img_url: function() {
        const [options, path] = getopts(arguments);
        const baseurl = options.data.root.config.urls.base;
        var outputUrl = new URL(path, baseurl).href;
        if (!options.hash.absolute)
            outputUrl = outputUrl.substring(baseurl.length);
        outputUrl = encodeURI(outputUrl);
        
        return new Handlebars.SafeString(outputUrl);
    },
    encode: function(string, options) {
        var uri = string || options;
        return new Handlebars.SafeString(encodeURIComponent(uri));
    },
    // meta_title
    url: function() {
        const [options, path] = getopts(arguments);
        const baseurl = options.data.root.config.urls.base;
        var outputUrl = new URL(path || this.path, baseurl);

        // FIXME: is absolute /blah or https://foo/blah?
	if (!options.hash.absolute)
            outputUrl = outputUrl.href;			    
        else
            outputUrl = outputUrl.href.substring(outputUrl.origin.length);
				    
        outputUrl = encodeURI(outputUrl);

        return new Handlebars.SafeString(outputUrl);
    },
    date: function() {
        var [options, date] = getopts(arguments);
        var timezone, format, timeago, timeNow, dateMoment;

        const root = options.data.root;
        if (!date) {
            timezone = root.config.timezone;

            // set to published_at by default, if it's available
            // otherwise, this will print the current date
            if (root.git) {
                date = moment(root.git.authored).tz(timezone).format();
            }
        }

        // ensure that context is undefined, not null, as that can cause errors
        date = date === null ? undefined : date;

        format = options.hash.format || 'MMM DD, YYYY';
        timeago = options.hash.timeago;
        timezone = this.timezone;
        timeNow = moment().tz(timezone);

        // i18n: Making dates, including month names, translatable to any language.
        // Documentation: http://momentjs.com/docs/#/i18n/
        // Locales: https://github.com/moment/moment/tree/develop/locale
        dateMoment = moment(date);
        //dateMoment.locale(i18n.locale()); // Commented to avoid i18n dep

        if (timeago) {
            date = timezone ? dateMoment.tz(timezone).from(timeNow) : dateMoment.fromNow();
        } else {
            date = timezone ? dateMoment.tz(timezone).format(format) : dateMoment.format(format);
        }

        return new Handlebars.SafeString(date);
    },
};
