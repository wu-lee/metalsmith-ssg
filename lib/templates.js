'use strict';
/** Defines functions which map registration data into HTML email
 * content for user acknowledgements and admin notifications.
 */

const handlebars = require('handlebars');
const fs = require('fs');
const handlebarsHelpers = require('./helpers.js');
const config = require('./config.js');
const maillog = require('./logging.js').loggers.mail;

// Register the helpers - we use the same ones as supplied to the
// theme, out of laziness.
for(var name in handlebarsHelpers) {
    handlebars.registerHelper(name, handlebarsHelpers[name]);
}

function defineTemplateLoader(file, name, defaultTemplate) {
    if (!file) {
        maillog.warn(`no configured ${name} template file, using default`);
        return defaultTemplate;
    }
    maillog.info(`${name} will be loaded from the file: ${file}`);
    return (context) => {
        if (fs.existsSync(file)) {
            maillog.info(`loading ${name} from file: ${file}`);
            const content = fs.readFileSync(file, 'utf8');
            return handlebars.compile(content)(context);
        }
        else {
            maillog.warn(`using default as no ${name} found at ${file}`);
            return defaultTemplate(context);
        }
    };    
}


// Define functions which returns the email templates.  These are
// re-loaded on each send so that they can be defined by files in the
// source (with inline handlebars markup). (These templates can be
// excluded from appearing in the output wiki by various means.)  The
// one required field in submitted data is an 'email' field with a
// recipient address. All fields, plus parts of the config, are
// supplied to the templates.
var notificationTemplate, acknowledgementTemplate, issueTemplate;
if (config.email && config.email.templates) {
    notificationTemplate = config.email.templates.notification;
    acknowledgementTemplate = config.email.templates.acknowledgement;
}
if (config.gitlab && config.gitlab.registration && config.gitlab.registration) {
    issueTemplate = config.gitlab.registration.description;
}

module.exports = {
    notification: defineTemplateLoader(
        notificationTemplate,
        'email notification template',
        (context) =>
            `<pre>${JSON.stringify(context.request.body, null, 2)}</pre>`
    ),
    acknowledgement: defineTemplateLoader(
        acknowledgementTemplate,
        'email acknowledgement template',
        (context) => 'Thanks! We shall be in touch.'
    ),
    issue: defineTemplateLoader(
        issueTemplate,
        'registration issue template',
        (context) =>
            'Form Data:\n\n```'+JSON.stringify(context.request.body, null, 2)+'```'
    ),
};
